/**
 * @author Van de Voorde Siebe
 * @Version 18-10-2021
 */
;(function () {
    "use strict";


    let express = require("express");
    let app = express();
    let port = 3000;

    app.use(function(req, res , next){
        console.log(`${new Date()} - ${req.method} request for ${req.url}`);
        next();
    });

    app.use(express.static("../static"));

    app.listen(port, function () {
        console.log(`Serving static on http://localhost:${port}`)
    });

    })();